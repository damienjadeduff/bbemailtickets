#!/usr/bin/env python3

"""

send_tickets_bb.py

By Damien Jade Duff, Istanbul Technical University

Here is a program for sending survey tickets anonymously. 
This program does not generate the tickets. 
It takes them as a plain text file,
along with a file containing the email addresses to 
which the tickets will be sent. The tickets file 
also contains links to the relevant
survey page and the survey title. 
This is all configured by the input conf file.

The main reason this script is provided public 
is so that students can inspect the code used to
ensure their anonymity during survey-taking.

It is likely others will want to use this script 
as it contains code to send OpenPGP-signed emails
using pygpgme, something which was lacking 
at the time of writing. This can be used with 
little adaptation. Hence, the below license.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import sys
import csv
import random
import os
import configparser
import argparse
import io

import email
import email.message
import email.mime
import email.mime.text
import email.mime.multipart
import smtplib
import getpass

try:
    import gpgme
    gpg_obj = gpgme.Context()
    gpg_obj.armor = True
    gpg_obj.textmode = True
except Exception as e:
    print('NOTE: pypgpme for Python 3 is not available in your current environment. Error was:',e)
    print('Perhaps try installing it? Like this:\nsudo apt-get install python3-gpgme')
    sys.exit(1)


def make_email(to_name,to_address,subject,message_text,
               from_name='',from_address='nobody@nowhere',
               signed=False,password=''):
   
    email_building = email.mime.multipart.MIMEMultipart(_subtype='signed',
                                                        micalg='pgp-sha256',
                                                        protocol='application/pgp-signature')

    mime_message = email.mime.text.MIMEText(message_text)
    email_building.attach(mime_message)
  
    email_building['Subject'] = subject
    if from_name!='':
        email_building['From'] = from_name + ' <' + from_address + '>'
    else:
        email_building['From'] = from_address
    if to_name!='':
        email_building['To'] = to_name + ' <' + to_address + '>'
    else:
        email_building['To'] = to_address
    
    if signed:

        msgIO = io.BytesIO(mime_message.as_bytes())
        sigIO = io.BytesIO()
        sig = gpg_obj.sign(msgIO,sigIO,gpgme.SIG_MODE_DETACH)
        message_signature = sigIO.getvalue().decode('ascii')
        
        try:
            assert(message_signature)
        except Exception as e:
            print('Problem signing message.')
            print(e)
            sys.exit(1)
        
        signed_part = email.message.Message()
        signed_part ['Content-Type'] = 'application/pgp-signature; name="signature.asc"'
        signed_part ['Content-Description'] = 'OpenPGP digital signature'
        signed_part.set_payload(str(message_signature))            
            
        email_building.attach(signed_part)    
    
    return email_building
    

def process_config(config):
    
    conf_file = config['meta']['conf_file']
    conf_dir = config['meta']['conf_dir']

    print('Processing configuration file:',conf_file)

    path_to_message_text = os.path.join(conf_dir,config['files']['message_text'])
    path_to_tickets = os.path.join(conf_dir,config['files']['tickets'])
    path_to_email_list = os.path.join(conf_dir,config['files']['email_list'])
    
    sender_email = config['email']['sender_email']
    sender_name = config['email']['sender_name']
    
    send_emails = config['email'].getboolean('send_emails',True)

    smtp_server_address = config['smtp']['smtp_server']
    smtp_username = config['smtp'].get('smtp_username','')
    smtp_starttls = config['smtp'].getboolean('smtp_starttls',False)

    gpg_sign_msg = config['gpg'].getboolean('sign')
    gpg_terminal_pw_prompt = config['gpg'].getboolean('terminal_pw_prompt',False)

    try:
        fh_message_text = open(path_to_message_text, 'r')
        fh_tickets = open(path_to_tickets, 'r')
        reader_tickets = csv.reader(fh_tickets,delimiter=',')
        fh_email_list = open(path_to_email_list, 'r')
        reader_email_list = csv.reader(fh_email_list,delimiter=',')
    except Exception as e:
        print('Could not open a file mentioned in',conf_file)
        print(e)
        sys.exit(1)
    
    message_text_template = fh_message_text.read()
    
    tickets = []
    ticket_urls = []
    ticket_coursenames = []
    email_addresses = []
    
    zero_line_encountered = False
    
    # We read tickets & addresses in to memory because we wish to check the number 
    # before sending & the size of this data is negligible.
    for line_cntr,row in enumerate(reader_tickets):
        if len(row)>0:
            if zero_line_encountered:
                print('Weirdness in input data - some lines contain no data. Line',
                      line_cntr-1,'... File',path_to_tickets,'.')
                print("This is too weird for me - I'm out.")
                sys.exit(1)
            tickets.append(row[0])
            ticket_urls.append(row[1])
            ticket_coursenames.append(row[2])
        else:
            zero_line_encountered = True
    
    zero_line_encountered = False
    line_cntr = 0
    
    for line_cntr,row in enumerate(reader_email_list):
        if len(row)>0:
            if zero_line_encountered:
                print('Weirdness in input data - some lines contain no data. Line',
                      line_cntr-1,'... File',path_to_email_list,'.')
                print("This is too weird for me - I'm out.")
                sys.exit(1)
            email_addresses.append(row[0])
        else:
            zero_line_encountered = True
        
    assert(len(tickets)==len(email_addresses))
    
    print('All files loaded for configuration',conf_file)
        
    if send_emails:
        
        print('Opening SMTP server connection to '+smtp_server_address)
        
        try:
            smtp_server = smtplib.SMTP(smtp_server_address) 
        except Exception as e:
            print('Could not open SMTP connection.')
            print(e)
            sys.exit(1)
            
        if smtp_username!='':
            print('This SMTP server seems to require a login (because you specified a user name).')
            smtp_password = getpass.getpass('First, please enter your SMTP Server password (address: '
                +smtp_server_address+', username: '+smtp_username+'): ')
            if smtp_starttls:
                try:
                    smtp_server.starttls()
                except Exception as e:
                    print('Server does not seem to support STARTTLS. Something wrong, somewhere.')
                    print(e)
                    sys.exit(1)
            try:
                smtp_server.login(smtp_username,smtp_password)
            except Exception as e:
                print("Couldn't log in to SMTP.")
                print(e)
                sys.exit(1)
        
        input("Press a key to continue - this is not a dry run - emails *will* be sent.")
        
        
    if gpg_sign_msg:
        print('You have asked for the message to be signed.')
        if gpg_terminal_pw_prompt:
            gpg_password = getpass.getpass('Now, please enter your GPG Passphrase: ')
        else:
            gpg_password=''
            print('GPG may pop up a window asking for your password.')
    else:
        gpg_password=''

    print('Now automatically sending emails.')  

    #!!!!!!!!!!!!!!!
    # This is the line that provides the anonymity.
    random.shuffle(email_addresses)
    #!!!!!!!!!!!!!!!

    for email_add,ticket_no,target_url,coursename in zip(email_addresses,tickets,ticket_urls,ticket_coursenames):
        
        print('Preparing email to',email_add)
        message_text = message_text_template
        message_text = message_text.replace('%%ticket%%',ticket_no)
        message_text = message_text.replace('%%link%%',target_url)
        message_text = message_text.replace('%%coursename%%',coursename)
    
        message = make_email('',email_add,'Your survey ticket for course "'+coursename+'"',
                             message_text,from_name=sender_name,from_address=sender_email,
                             signed=gpg_sign_msg,password=gpg_password)
        
        if send_emails:
            print('Sending email to',email_add)
            smtp_server.sendmail(sender_email,[email_add],message.as_string(unixfrom=True))
        else:
            print()
            print('***********************************************************************************')
            print('**** WOULD HAVE BEEN SENT FROM',sender_email,'TO',email_add)
            print('***********************************************************************************')
            print(message.as_string(unixfrom=True))
            print()
            fname = 'EMAIL_TICKET_'+email_add+'_'+coursename+'.eml'
            fname = fname.replace('+','_plus_').replace(' ','_space_').replace(':','_colon_').replace('@','_at_')
            with open(fname,'w') as f:
                f.write(message.as_string(unixfrom=True))
            # Note that the above printing does not cause any problem for anonymity because it only happens if 
            # the emails are NOT being sent (and they are shuffled every time).
    


if __name__=='__main__':

    arg_parser = argparse.ArgumentParser(description='Send tickets to students anonymously.')

    arg_parser.add_argument('conf_files', help='The configuration file(s) for the job.',nargs='+')

    args = arg_parser.parse_args()

    configs = []
    
    for conf_file in args.conf_files:

        try:
            print('Configuration file provided:' , conf_file)
            conf_dir = os.path.dirname(conf_file)
            config = configparser.ConfigParser()
            with open(conf_file,'r') as f: 
                # reading the file myself because configparser doesn't like to throw exceptions
                config.readfp(f)
                config['meta']={}
                config['meta']['conf_dir']=conf_dir
                config['meta']['conf_file']=conf_file
                configs.append(config)
        except Exception as e:
            print('Error reading conf file:')
            print(e)
            sys.exit(1)
    
    for config in configs:
        process_config(config)
        
    print("Program successfully finished.")
    
    