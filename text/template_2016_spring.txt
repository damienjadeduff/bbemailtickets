The English text is below.

--------------

Sevgili Öğrenci,

Bu mesajda "%%coursename%%" dersinin 2016 Bahar dönemi için anonim dağıtılan bir anket bulunmaktadır.
Lütfen aşağıdaki linke basarak dersin anket sayfasına git, verilen bilet numarasını ilgili alana gir ve anketi doldur.

"%%coursename%%" dersinin anket sayfası linki: %%link%%
Anket numarası: %%ticket%%

Bu anket Bilgisayar Mühendisliği Bölümü'nün sizin ve sizden sonra gelecek öğrencilerin eğitimlerini ölçmek ve ilerletmek için yaptığı süregelen çalışmaların bir parçasıdır. Senin de bu çalışmalara katkı vereceğini umuyoruz.

Bu mesaj sana kaynak kodunu şu adreste görebileceğin bir anonim program tarafından yollandı:
https://bitbucket.org/damienjadeduff/bbemailtickets/src
Mesajın göndericisi bu mesajı gönderen program ile yukarıdaki adreste görülebilecek programın aynı olduğunu beyan eder.
Ayrıca bu mesaja göndericinin kimliğinin doğrulanmasını sağlayan bir OpenPGP imzası da iliştirilmiştir.

Saygıyla,
İTÜ Bilgisayar Mühendisliği Bölümü Ölçme ve Değerlendirme Komisyonu

--------------

Dear Student,

This email contains an anonymously distributed survey ticket for the course "%%coursename%%" for the 2016 Spring term. 
Please click on the below link to go to the survey page for the course, enter the provided ticket number there, and fill out the survey.

Survey page link for course "%%coursename%%": %%link%%
Ticket number: %%ticket%%

This survey is a part of the Department of Computer Engineering's ongoing efforts to measure and improve your education and education of future students. We hope you will join in our efforts.

This email is being sent to you via an anonymous program that you can browse at the following link:
https://bitbucket.org/damienjadeduff/bbemailtickets/src
The sender of this email declares that the program used to send these emails is the same as that viewable above.
Attached to the current email is also an OpenPGP signature verifying the identity of the sender.

Best regards,
ITU Computer Engineering Department Measurement and Evaluation Commission

