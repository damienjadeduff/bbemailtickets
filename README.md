``sent_tickets_bb.py``
======================

This code is intended to be inspected by students who wish to see how their survey tickets are being anonymised.

For more information, see the file ``src/send_tickets_bb.py``.

**This has been tested on Kubuntu 16.04 and has not been designed with other OSs in mind.**